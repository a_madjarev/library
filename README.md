# Summer Workshop 2019
## The Project "Library"
Library is simple web application design to represent virtual library. Project was intentionally kept simple ie. it has only few entities but it shows usage of modern web technologies.
Project will be organized in several sections (named steps) where each one represents the final stage of the finished section.
## Course Plan

This course will be organized as a 5 day event. Each day will consist of introductory phase and development. Students will also have assignments determined by the project phase to straighten up their knowledge about the technologies used.

Project development steps/labs are divided between days:  
1. Day 01: Introduction, environment setup, startup application using node.js with express framework  
2. Day 02: Continuation of day 1 and building REST Api for our backend application  
3. Day 03: Frontend: HTML5, CSS3, Bootstrap, Angular, Form validation, Error messages  
4. Day 04: Continuation of day 3  
5. Day 05: Adding security layer to our applications  

[**On Day 01** we will concentrate on setup of the development environment, install various needed software to successfully finish the project course. The links will be provided in the README document of day 01. We will also create our startup application, install necessary dependencies, connect to database, and prepare everything to build our REST Api. You will also get the chance to clone the project code from repository and build/run complete project so that you can see the scope of the project.](https://bitbucket.org/a_madjarev/library/src/master/Tech9/day1/)

[**On Day 02** we will continue development of the project creating our REST Api. We will create necessary endpoints which we will use for access and manipulation of necessary data in our MongoDB database. We will create necessary Models and also add validation layer that we are going to use in our API calls.](https://bitbucket.org/a_madjarev/library/src/master/Tech9/day2/)

[**On Day 03**, after adding REST endpoints we proceed to frontend implementation - graphical interface that will communicate with endpoints.](https://bitbucket.org/a_madjarev/library/src/master/Tech9/day3/)

[**On Day 04** we are going to continue development of frontend application using Angular.](https://bitbucket.org/a_madjarev/library/src/master/Tech9/day4/)

[**On Day 05** we will wrap up the course with all previous phases and add security layer to our application.](https://bitbucket.org/a_madjarev/library/src/master/Tech9/day5/)

## Setup information
To run the project you should clone the repository in directory of your choosing. Before running the project, project needs to be installed. Run `npm install` in root of the application, and also in the frontend subfolder.  
* To run the backend application run `npm start server` in project root.  
* To run the frontend application run `ng serve` in frontend subfolder (ng-library).  

Our backend is running locally on port 5000, and our frontend on port 4200.

## Technologies that we going to use
* Git
* ES6+ JavaScript
* TypeScript
* Node.js / Express
* MongoDB
* Angular
* Twitter Bootstrap

## Further reading, documentation and free courses

## REST Api Documentation
Our Api is running on port 5000, on local environment base url for API will be http://localhost:5000
Our end points will be accessible on this base url

### Open Endpoints
Open endpoints require no Authentication

### Closed Endpoints
Closed endpoints require a valid Token to be included in the header of the request. A Token can be acquired from the Login endpoint.

#### Account related
Endpoints for registration, login and getting info of logged in user.  
- [Register an User:](https://bitbucket.org/a_madjarev/library/src/master/Api%20Documentation/User/register.md) `POST /api/users/register`  
- [Login an User:](https://bitbucket.org/a_madjarev/library/src/master/Api%20Documentation/User/login.md) `POST /api/users/login`  
- [Get current User:](https://bitbucket.org/a_madjarev/library/src/master/Api%20Documentation/User/current.md) `GET /api/users/current`  

#### Category related
Endpoints for categories accessing and manipulation  
- [Get All Categories:](https://bitbucket.org/a_madjarev/library/src/master/Api%20Documentation/Category/getAll.md) `GET /api/categories`  
- [Get Category by ID:](https://bitbucket.org/a_madjarev/library/src/master/Api%20Documentation/Category/getById.md) `GET /api/categories/:cat_id`  
- [Create or Update Category:](https://bitbucket.org/a_madjarev/library/src/master/Api%20Documentation/Category/post.md) `POST /api/categories`  
- [Delete a Category:](https://bitbucket.org/a_madjarev/library/src/master/Api%20Documentation/Category/delete.md) `DELETE /api/categories/:cat_id`  

#### Books related
Endpoints for books accessing and manipulation  
- [Get All Books:](https://bitbucket.org/a_madjarev/library/src/master/Api%20Documentation/Book/getAll.md) `GET /api/books`  
- [Get All Books related to some Category:](https://bitbucket.org/a_madjarev/library/src/master/Api%20Documentation/Book/getAllByCategoryId.md) `GET /api/books/category/:cat_id`  
- [Get Book by ID:](https://bitbucket.org/a_madjarev/library/src/master/Api%20Documentation/Book/getById.md) `GET /api/books/:book_id`  
- [Create or Update Book:](https://bitbucket.org/a_madjarev/library/src/master/Api%20Documentation/Book/post.md) `POST /api/books`  
- [Delete a Book:](https://bitbucket.org/a_madjarev/library/src/master/Api%20Documentation/Book/delete.md) `DELETE /api/books/:book_id`  