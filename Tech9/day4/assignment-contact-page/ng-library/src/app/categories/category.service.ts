import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';

import { Category } from './category.model';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  API = 'http://localhost:5000/api/categories';
  categoryFilterChanged = new Subject<number>();

  constructor(private httpClient: HttpClient) { }

  // Return Observable that wraps array of Categories
  getCategories(): Observable<Category[]> {
    return this.httpClient.get<Category[]>(this.API);
  }

  // Update category if book already has an ID, save it otherwise and return Observable
  saveCategory(category: Category): Observable<any> {
    return this.httpClient.post(this.API, category);
  }

  // Delete category by ID and return Observable
  deleteCategory(categoryId: number) {
    return this.httpClient.delete(this.API + '/' + categoryId);
  }
}
