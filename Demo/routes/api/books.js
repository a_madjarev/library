const express = require("express");
const router = express.Router();
const passport = require("passport");
const allowOnly = require("./middleware/roleChecker").allowOnly;
const roles = require("../../config/roles");
const ObjectId = require("mongoose").Types.ObjectId;
const ISBN = require("is-isbn");

// Load Validation
const validateBookInput = require("../../validation/books");

// Load models
const Book = require("../../models/Book");

// @route   GET api/books/test
// @desc    Tests post route
// @access  Public
router.get("/test", (req, res) => {
  res.json({msg: "Books works"});
});

// @route   GET api/books/
// @desc    Find All Books
// @access  Private (User, Admin)
router.get(
  "/",
  passport.authenticate("jwt", {session: false}),
  allowOnly(roles.accessLevels.user, (req, res) => {
    let errors = {};
    Book.find()
      .populate('category', ['name'])
      .then(books => {
        if (books.length === 0) {
          errors.no_books = "There are no books";
          return res.status(400).json(errors);
        }

        return res.status(200).json(books);
      })
      .catch(err => {
        return res.status(404).json({
          msg: "There was an error fetching books from database",
          err: err
        });
      });
  })
);

// @route   GET api/books/category/:cat_id
// @desc    Find Books By Category Id
// @access  Private (User, Admin)
router.get(
  "/category/:cat_id",
  passport.authenticate("jwt", {session: false}),
  allowOnly(roles.accessLevels.user, (req, res) => {
    let errors = {};
    Book.find({category: ObjectId(req.params.cat_id)})
      .populate('category', ['name'])
      .then(books => {
        if (books.length === 0) {
          errors.no_books = "Requested books does not exists";
          return res.status(400).json(errors);
        }

        return res.status(200).json(books);
      })
      .catch(err => {
        return res.status(404).json({
          msg: "There was an error fetching books from database",
          err: err
        });
      });
  })
);

// @route   GET api/books/:book_id
// @desc    Find Book By Id
// @access  Private (User, Admin)
router.get(
  "/:book_id",
  passport.authenticate("jwt", {session: false}),
  allowOnly(roles.accessLevels.user, (req, res) => {
    let errors = {};
    Book
      .findOne({_id: ObjectId(req.params.book_id)})
      .populate('category', ['name'])
      .then(books => {
        if (!books) {
          errors.no_book = "Requested book does not exists";
          return res.status(400).json(errors);
        }

        return res.status(200).json(books);
      })
      .catch(err => {
        return res.status(404).json({
          msg: "There was an error fetching book from database",
          err: err
        });
      });
  })
);

// @route   POST api/books/
// @desc    Create Or Update A Book
// @access  Private (Admin)
router.post(
  "/",
  passport.authenticate("jwt", {session: false}),
  allowOnly(roles.accessLevels.admin, (req, res) => {
    const {errors, isValid} = validateBookInput(req.body);

    // Check Validation
    if (!isValid) {
      return res.status(400).json(errors);
    }

    if (ISBN.validate(req.body.isbn)) {
      let bookFields = {};
      bookFields.title = req.body.title;
      bookFields.author = req.body.author;
      bookFields.publishDate = req.body.publishDate;
      bookFields.isbn = req.body.isbn;
      bookFields.category = req.body.category;

      Book
        .findOne({_id: ObjectId(req.body._id)})
        .then(book => {
          if (book) {
            Book.findOneAndUpdate(
              {_id: ObjectId(req.body._id)},
              {$set: bookFields},
              {new: true}
            )
              .then(books => {
                return res.status(200).json(books);
              })
              .catch(err => {
                return res.status(404).json({
                  msg: "There was an error updating book",
                  err: err
                });
              });
          } else {
            new Book(bookFields)
              .save()
              .then(book => {
                return res.status(200).json(book);
              })
              .catch(err => {
                return res.status(404).json({
                  msg: "There was an error saving new Book",
                  err: err
                });
              });
          }
        });
    } else {
      errors.isbn = "Isbn is not valid";
      return res.status(400).json(errors);
    }
  })
);

// @route   DELETE api/books/:book_id
// @desc    Delete Book By Id
// @access  Private (Admin)
router.delete(
  "/:book_id",
  passport.authenticate("jwt", {session: false}),
  allowOnly(roles.accessLevels.admin, (req, res) => {
    Book.findOneAndRemove({_id: ObjectId(req.params.book_id)})
      .then((result) => {
        if (result) {
          return res.status(200).json({msg: "Book removed"});
        }
        return res.status(400).json({msg: "Book does not exists"});
      })
      .catch(err => {
        return res.status(404).json({
          msg: "There was an error removing book",
          err: err
        });
      });
  })
);

module.exports = router;
