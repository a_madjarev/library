const express = require("express");
const router = express.Router();

// @route   GET api/books/test
// @desc    Tests post route
// @access  Public
router.get("/test", (req, res) => {
  res.json({msg: "Books works"});
});

module.exports = router;
