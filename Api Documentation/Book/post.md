Create new Book

**URL** : `/api/book/`

**Method** : `POST`

**Auth required** : YES

**Permissions required** : ADMIN

**Data constraints**

Provide title, category object, author, isbn and publish date of Book to be created.

```json
{
    "title":"Caliban's War",
    "author":"James S. A. Corey",
    "publishDate":"2012-06-25T22:00:00.000+00:00",
    "isbn":"9788834729083",
    "category": {
        "_id": "5d01f7617158ac2bcfb5f884",
        "name": "Sci-fi"
    }
}
```

## Success Response

**Condition** : Book is created

**Code** : `200 OK`

**Content example**

```json
{
    "_id": "5d03987cc899b560ed1a9f73",
    "title": "Caliban's War",
    "author": "James S. A. Corey",
    "publishDate": "2012-06-25T22:00:00.000Z",
    "isbn": "9788834729083",
    "category": "5d01f7617158ac2bcfb5f884",
    "date": "2019-06-14T12:52:12.487Z",
    "__v": 0
}
```

If Book is updating, then ID is also present in the payload

**Data constraints**
```json
{
    "_id": "5d03987cc899b560ed1a9f73",
    "title":"Caliban's War",
    "author":"James S. A. Corey",
    "publishDate":"2012-06-25T22:00:00.000+00:00",
    "isbn":"9788834729083",
    "category": {
        "_id": "5d01f7617158ac2bcfb5f884",
        "name": "Sci-fi"
    }
}
```

## Success Response

**Condition** : Category is updated

**Code** : `200 OK`

**Content example**

```json
{
    "_id": "5d03987cc899b560ed1a9f73",
    "title": "Caliban's War",
    "author": "James S. A. Corey",
    "publishDate": "2012-06-25T22:00:00.000Z",
    "isbn": "9788834729083",
    "category": "5d01f7617158ac2bcfb5f884",
    "date": "2019-06-14T12:52:12.487Z",
    "__v": 0
}
```

## Error Responses

**Condition** : If fields are missed.

**Code** : `400 BAD REQUEST`

**Content example**

```json
{
    "title": "Title field is required",
    "author": "Author field is required",
    "publishDate": "Publish Date field is required",
    "Isbn": "Isbn field is required"
}
```

### Or

**Condition** : If ISBN is invalid

**Code** : `400 BAD REQUEST`

**Content example**

```json
{
    "isbn": "Isbn is not valid"
}
```
