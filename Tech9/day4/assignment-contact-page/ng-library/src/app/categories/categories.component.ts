import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { NgForm } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';

import { Category } from './category.model';
import { CategoryService } from './category.service';

@Component({
  selector: 'library-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {
  categories$: Observable<Category[]>;
  selectedCategory: Category = { _id: null, name: null };
  @ViewChild('f', {static: false}) addCategoryForm: NgForm;
  error: { name: string };
  operation: string;

  constructor(private categoryService: CategoryService) { }

  ngOnInit() {
    this.categories$ = this.categoryService.getCategories();
  }

  onCategoryDelete(category: Category) {
    this.selectedCategory = Object.assign({}, category);
  }

  onCategoryDeleteSubmit() {
    this.categoryService.deleteCategory(this.selectedCategory._id)
    .subscribe(
      () => {
        this.categories$ = this.categoryService.getCategories();
        this.selectedCategory = null;
      },
      (error) => console.error(error)
    );
  }

  onCategoryAdd() {
    this.operation = 'Add';
    this.addCategoryForm.reset();
    this.selectedCategory = { _id: null, name: null };
    this.error = null;
  }

  onCategoryEdit(category: Category) {
    this.operation = 'Edit';
    this.selectedCategory = Object.assign({}, category);
    this.error = null;
  }

  onCategorySaveSubmit(form: NgForm, closeButton: HTMLButtonElement) {
    this.categoryService.saveCategory({
      _id: this.operation === 'Add' ? null : this.selectedCategory._id,
      name: form.value.name
    })
      .subscribe(
        () => {
          this.categories$ = this.categoryService.getCategories();
          closeButton.click();
        },
        (httpErrorResponse: HttpErrorResponse) => {
          this.error = httpErrorResponse.error;
        }
      );
  }

}
