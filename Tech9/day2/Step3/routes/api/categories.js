const express = require("express");
const router = express.Router();
const ObjectId = require("mongoose").Types.ObjectId;

// Load Validation
const validateCategoryInput = require("../../validation/categories");

// Load models
const Category = require("../../models/Category");
const Book = require("../../models/Book");

// @route   GET api/categories/test
// @desc    Tests post route
// @access  Public
router.get("/test", (req, res) => {
  res.json({msg: "Categories works"});
});

// @route   GET api/categories/
// @desc    Find All Categories
// @access  Private (User, Admin)
router.get(
  "/",
  (req, res) => {
    let errors = {};
    Category.find()
      .then(categories => {
        if (categories.length === 0) {
          errors.no_categories = "There are no categories";
          return res.status(400).json(errors);
        }

        return res.status(200).json(categories);
      })
      .catch(err => {
        return res.status(404).json({
          msg: "There was an error fetching categories from database",
          err: err
        });
      });
  }
);

// @route   GET api/categories/:cat_id
// @desc    Find Category By Id
// @access  Private (User, Admin)
router.get(
  "/:cat_id",
  (req, res) => {
    let errors = {};
    Category.findOne({_id: ObjectId(req.params.cat_id)})
      .then(category => {
        if (!category) {
          errors.no_category = "Requested category does not exists";
          return res.status(400).json(errors);
        }

        return res.status(200).json(category);
      })
      .catch(err => {
        return res.status(404).json({
          msg: "There was an error fetching category from database",
          err: err
        });
      });
  }
);

// @route   POST api/categories/
// @desc    Create Or Update Category
// @access  Private (Admin)
router.post(
  "/",
  (req, res) => {
    const {errors, isValid} = validateCategoryInput(req.body);

    // Check Validation
    if (!isValid) {
      return res.status(400).json(errors);
    }

    let categoryFields = {};
    categoryFields.name = req.body.name;

    Category.findOne({_id: ObjectId(req.body.id)}).then(category => {
      if (category) {
        Category.findOneAndUpdate(
          {_id: ObjectId(req.body.id)},
          {$set: categoryFields},
          {new: true}
        )
          .then(category => {
            return res.status(200).json(category);
          })
          .catch(err => {
            return res.status(404).json({
              msg: "There was an error updating category",
              err: err
            });
          });
      } else {
        new Category(categoryFields)
          .save()
          .then(category => {
            return res.status(200).json(category);
          })
          .catch(err => {
            return res.status(404).json({
              msg: "There was an error saving new Category",
              err: err
            });
          });
      }
    });
  }
);

// @route   DELETE api/categories/:cat_id
// @desc    Delete Category By Id
// @access  Private (Admin)
router.delete(
  "/:cat_id",
  (req, res) => {
    // Remove all books related to that category
    Book
      .deleteMany({'category': ObjectId(req.params.cat_id)})
      .then(() => {
        Category.findOneAndRemove({_id: ObjectId(req.params.cat_id)})
          .then((result) => {
            if (result) {
              return res.status(200).json({msg: "Category removed"});
            }
            return res.status(400).json({msg: "Category does not exists"});
          })
          .catch(err => {
            return res.status(404).json({
              msg: "There was an error removing category",
              err: err
            });
          });
      })
      .catch(err => {
        return res.status(404).json({
          msg: "There was an error removing books",
          err: err
        });
      });
  }
);

module.exports = router;