import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'library-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  onContactSubmit(form: NgForm) {
    console.log(form.value.email + '\n' + form.value.message);
    alert(form.value.email + '\n' + form.value.message);
  }

}
