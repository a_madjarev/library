const chai = require('chai');
const chaiHttp = require('chai-http');
const request = require('supertest');
const app = require('../../server');

const Book = require('../../models/Book');

let should = chai.should();

chai.use(chaiHttp);

describe('Testing of the get all books route GET /api/books', () => {
  it('should return status 200', (done) => {
    chai.request(app)
      .get('/api/books')
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a('array');
        done();
      });
  });
});

describe('Testing of the creating new book route POST /api/book', () => {
  it('should create new book, and return status 200', (done) => {
    request(app)
      .post('/api/books')
      .send({
        "title": "new test book",
        "category": "5cfe3d17da3d9c7c84a2caae",
        "author": "test author",
        "isbn": "9780747409878",
        "publishDate": "Mon Jun 10 2019 13:46:13 GMT+0200"
      })
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.have.property('title');
        res.body.should.have.property('title').eql('new test book');
        res.body.should.have.property('author');
        res.body.should.have.property('author').eql('test author');
        res.body.should.have.property('category');
        res.body.should.have.property('isbn');
        res.body.should.have.property('publishDate');
        done();
      });
  });
});

describe('Remove testing books', () => {
  it('should remove all books with title new test category', (done) => {
    Book.deleteMany({"title": "new test book"}, function (err) {
    });
    done();
  });
});