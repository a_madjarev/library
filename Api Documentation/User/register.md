# Register an User

Register a new User if that user doesn't exist.

**URL** : `/api/users/register`

**Method** : `POST`

**Auth required** : No

**Permissions required** : None

**Data constraints**

Provide name, email, password and password2 for an User.

```json
{
	"name": "Kayo Hinazuki",
	"email": "kayohinazuki@revival.com",
	"password": "bakanano",
	"password2": "bakanano"
}
```

## Success Response

**Condition** : If everything is OK and an Account is created for this User.

**Code** : `200 OK`

**Content example**

```json
{
    "_id": "5cfe3cbcbda5637c00ba88f9",
    "name": "Kayo Hinazuki",
    "email": "kayohinazuki@revival.com",
    "password": "$2a$10$h/ZyqU/jUB2Xu6hm/ZTmieXNyqTKdh9e1T3aGQ0k5lqwCB7W/lwU2",
    "role": 2,
    "date": "2019-06-10T11:19:24.736Z",
    "__v": 0
}
```

## Error Responses

**Condition** : If User with that email already exists.

**Code** : `400 BAD REQUEST`

**Content** : 
```json
{
    "email": "Email already exists"
}
```

### Or

**Condition** : Email is invalid format

**Code** : `400 BAD REQUEST`

**Content example**

```json
{
    "email": "Email is invalid"
}
```

### Or

**Condition** : If fields are missed.

**Code** : `400 BAD REQUEST`

**Content example**

```json
{
    "name": "Name field is required",
    "email": "Email field is required",
    "password": "Password field is required",
    "password2": "Confirm password field is required"
}
```

### Or

**Condition** : If some field don't have required number of characters

**Code** : `400 BAD REQUEST`

**Content example**

```json
{
    "name": "Name must be between 2 and 30 characters",
    "password": "Password must be at least 6 characters"
}
```

### Or

**Condition** : If passwords don't match

**Code** : `400 BAD REQUEST`

**Content example**

```json
{
    "password2": "Passwords must match"
}
```