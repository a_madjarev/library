Create new Category

**URL** : `/api/categories/`

**Method** : `POST`

**Auth required** : YES

**Permissions required** : ADMIN

**Data constraints**

Provide name of Category to be created.

```json
{
  "name": "Fantasy"
}
```

## Success Response

**Condition** : Category is created

**Code** : `200 OK`

**Content example**

```json
{
    "_id": "5d039139ce66823fdd125903",
    "name": "Fantasy",
    "date": "2019-06-14T12:21:13.940Z",
    "__v": 0
}
```

If category is updating, then ID is also present in the payload

**Data constraints**
```json
{
    "_id": "5d039139ce66823fdd125903",
    "name": "Sci-fi"
}
```

## Success Response

**Condition** : Category is updated

**Code** : `200 OK`

**Content example**

```json
{
    "_id": "5d039139ce66823fdd125903",
    "name": "Sci-fi",
    "date": "2019-06-14T12:21:13.940Z",
    "__v": 0
}
```

## Error Responses

**Condition** : If fields are missed.

**Code** : `400 BAD REQUEST`

**Content example**

```json
{
    "name": "Name field is required"
}
```
