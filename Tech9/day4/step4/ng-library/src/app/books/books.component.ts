import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { NgForm } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';

import { Book } from './book.model';
import { BookService } from './book.service';
import { Category } from '../categories/category.model';
import { CategoryService } from '../categories/category.service';

@Component({
  selector: 'library-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.scss']
})
export class BooksComponent implements OnInit {
  books$: Observable<Book[]>;
  categories$: Observable<Category[]>;
  @ViewChild('f', {static: false}) saveBookForm: NgForm;
  selectedBook: Book = new Book(null, null, null, null, null, null);
  error: { title: string, isbn: string, author: string, publishDate: string };

  constructor(private bookService: BookService, private categoryService: CategoryService) { }

  ngOnInit() {
    this.books$ = this.bookService.getBooks();
    this.categories$ = this.categoryService.getCategories();
  }

  onBookDelete(book: Book) {
    this.selectedBook = Object.assign({}, book);
  }

  onBookDeleteSubmit() {
    this.bookService.deleteBook(this.selectedBook._id)
      .subscribe(
        () => {
          this.books$ = this.bookService.getBooks();
          this.selectedBook = new Book(null, null, null, null, null, new Date(this.getCurrentDate()));
        },
        (error) => console.error(error)
      );
  }

  onBookAdd() {
    this.saveBookForm.reset(
      {publishDate: this.getCurrentDate()}
    );
    this.selectedBook = new Book(null, null, null, null, null, null);
    this.error = null;
  }

  onBookSaveSubmit(form: NgForm, closeButton: HTMLButtonElement) {
    const book: Book = new Book(
      null,
      form.value.isbn,
      form.value.category,
      form.value.title,
      form.value.author,
      form.value.publishDate
    );
    this.bookService.saveBook(book)
      .subscribe(
        () => {
          this.books$ = this.bookService.getBooks();
          closeButton.click();
        },
        (httpErrorResponse: HttpErrorResponse) => {
          this.error =  httpErrorResponse.error;
        }
      );
  }

  getCurrentDate() {
    return (new Date()).toISOString().slice(0, 10);
  }

}
