Day 5: Adding authentication
============================================================= 

Step 1 (backend) - Add authentication, roles and authorization
----------------------------------  
## Creating User Model, routes, and validation and introducing authentication  
- In config folder create two files passport.js and roles.js  
- Add passport middleware in server.js  
- Also modify keys.js and add secret property for authentication  
- In models folder create file called User.js  
- In validation folder create files login.js and register.js  
- In routes/api create folder middleware, and inside it create file roleChecker.js  
- In routes/api create file users.js  
- Create users using postman  
- Modify other routes to use authorization  

## Modify tests to use authorization
- Make appropriate changes in books.test.js and categories.test.js files


Step 2 (frontend) - Create Registration component and authorization service
----------------------------------- 
1. Generate "registration" component.
2. Add form element with input fields for name, email, password and confirmation password and "Register" button in "registration.component.html" template file.
3. Pass values from registration form to "onRegister()" method inside "registration.component.ts".
4. Create AuthService class with "register" function that takes name, email, password and password2 (confirmation password) as arguments and pass that as an object to http POST request to '/api/users/register' endpoint. This function should return observable as value.
5. Also add "isAuthenticated" function and "authenticated" property with 'false' as a default value.
6. In "registration.component.ts" add "AuthService" into constructor and crete "onRegister" function that will receive "NgForm" as an argument.
7. In "onRegister" function pass values from registration form to "register" function from "AuthService".
8. On successful registration, reset "error" object and show successful registration message by setting success property to 'true'.
9. If registration was unsuccessful assign "HttpErrorResponse" response error objet to "error" property and reset "success" property by setting it to 'false' value.
10. Add "registration" route to "app-routing.module.ts".
11. Modify header component template file to include "Register" link and to show links based on "authenticated" property from "AuthService" (inject "AuthService" into header component).
  
Step 3 (frontend) - Create Login component
----------------------------------- 
  
1. Generate "login" component.
2. Add form element with input fields for username, password and "Login" button in "login.component.html" template file.
3. Pass values from login form to "onLogin()" method inside "login.component.ts".
4. In "auth.service.ts" file create "login" function that takes "email" and "password" as arguments and pass them to login http POST request to check if user is authenticated and get the token if user is authenticated.
5. Create setter and getter functions for storing token in headers property.
6. If successfully authenticated set Authorization header with returned token as a value.
7. Create "getCurrentUser" function that will get user date from the API and call that function after getting the token from API.
8. Pass request headers, from "AuthService", on every request in book and category service.
9. Add functions in auth service for retrieving username and role for displaying username in header component and displaying links in header based on role of currently logged-in user.
10. Add "AuthService" to "header.component.ts" constructor.
11. Add login link, dropdown for logging out and change displaying of links based on current user role in header template.
12. Add "login" route to "app-routing.module.ts".
13. Add logout function in auth service that will clear all the user data and redirect user to the login page.

Step 4 (frontend) - Add auto login functionality and create auth-guard service to restrict unauthorized routes
----------------------------------- 

1. In "auth.service.ts" file, upon getting token, store the token in browser local storage and remove it upon logout.
2. Create "autoLogin" function for logging in user automatically with token from local storage upon refreshing the page.
3. In "app.component.ts" file inject "AuthService" and call autoLogin function in "ngOnInit" lifecycle hook.
4. Generate "auth.guard.ts" ("ng generate guard auth") that implements "CanActivate" interface and "canActivate" function.
5. "canActivate" function returns true if user is authenticated and have authorities to access route, otherwise it returns false and navigate to "/home" or "/login" route.
6. In "app-routing.module.ts" file define AuthGuard as a service for the canActivate property and pass required role for the given routes that needs to bi authorized.