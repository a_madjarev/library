# Get all categories

Get the list of all categories for authenticated users

**URL** : `/api/categories/`

**Method** : `GET`

**Auth required** : YES

**Permissions required** : None

## Success Response

**Code** : `200 OK`

**Content examples**

```json
[
    {
        "_id": "5cfe5b0660325b32eefa57eb",
        "name": "Horror",
        "date": "2019-06-10T13:28:38.853Z",
        "__v": 0
    },
    {
        "_id": "5cff5326576bb32602b14cf9",
        "name": "Fantasy",
        "date": "2019-06-11T07:07:18.327Z",
        "__v": 0
    },
    {
        "_id": "5cff5455576bb32602b14d03",
        "name": "Sci-fi",
        "date": "2019-06-11T07:12:21.428Z",
        "__v": 0
    },
    ...
]
```

## Error Responses

**Condition** : If there are no categories in database

**Code** : `400 BAD REQUEST`

**Content** : 
```json
{
    "no_categories": "There are no categories"
}
```