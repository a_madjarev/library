# Get all books from some category

Get the list of all books by category id

**URL** : `/api/books/category/:cat_id`

**Method** : `GET`

**Auth required** : YES

**Permissions required** : None

## Success Response

**Code** : `200 OK`

**Content examples**

```json
[
    {
        "_id": "5cff53e9576bb32602b14cff",
        "title": "The Eye of the World",
        "author": "Robert Jordan",
        "publishDate": "1990-01-14T23:00:00.000Z",
        "isbn": "9783453180000",
        "category": {
            "_id": "5cff5326576bb32602b14cf9",
            "name": "Fantasy"
        },
        "date": "2019-06-11T07:10:33.450Z",
        "__v": 0
    },
    {
        "_id": "5cff53f3576bb32602b14d01",
        "title": "The Great Hunt",
        "author": "Robert Jordan",
        "publishDate": "1990-11-14T23:00:00.000Z",
        "isbn": "9780606343190",
        "category": {
            "_id": "5cff5326576bb32602b14cf9",
            "name": "Fantasy"
        },
        "date": "2019-06-11T07:10:43.235Z",
        "__v": 0
    },
    ...
]
```

## Error Responses

**Condition** : If there are no books in database

**Code** : `400 BAD REQUEST`

**Content** : 
```json
{
    "no_books": "There are no books"
}
```