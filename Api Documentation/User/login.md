# Login an User

Login an User if that user exist.

**URL** : `/api/users/login`

**Method** : `POST`

**Auth required** : No

**Permissions required** : None

**Data constraints**

Provide email and password of an User.

```json
{
    "email": "admin@test.com",
    "password": "password"
}
```

## Success Response

**Condition** : If everything is OK and an Account exist for this User.

**Code** : `200 OK`

**Content example**

```json
{
    "success": true,
    "token": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVjZmUzY2JjYmRhNTYzN2MwMGJhODhmOSIsIm5hbWUiOiJBZG1pbiBBZG1pbiIsImlhdCI6MTU2MDQ5MjcwMiwiZXhwIjoxNTYwNTc5MTAyfQ.vuJ4ccPHUJF4ayAUBnIELHQPPNfHPfee961LpVnWYA4"
}
```

## Error Responses

**Condition** : If Account don't exists for User.

**Code** : `400 BAD REQUEST`

**Content** : 
```json
{
    "msg": "User not found"
}
```

### Or

**Condition** : If Password is incorrect.

**Code** : `400 BAD REQUEST`

**Content** : 
```json
{
    "password": "Password incorrect"
}
```

### Or

**Condition** : Email is invalid format

**Code** : `400 BAD REQUEST`

**Content example**

```json
{
    "email": "Email is invalid"
}
```

### Or

**Condition** : If fields are missed.

**Code** : `400 BAD REQUEST`

**Content example**

```json
{
    "email": "Email field is required",
    "password": "Password field is required"
}
```