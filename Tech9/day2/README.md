Day 2: Building REST Api for our backend application
=================================================================================

## Creating Category model, routes and validation  
- Add bodyParser middleware in server.js  
- Create models folder in root of project, and inside it create file called Category.js  
- Create validation folder in root of project, and inside it create two files called is-empty.js and categories.js  
- In routes/api add routes for categories in categories.js file  
- Test with postman  

## Creating Book model, routes and validation  
- In models folder create file called Book.js  
- In validation folder create file called books.js  
- In routes/api folder add routes for books in books.js file  
- Test with postman  

## Add unit tests  
- Add jest dependency with `npm install --save-dev mocha chai chai-http supertest`    
- Update package.json and add test script  
- in routes/api/ create two files: books.test.js and categories.test.js and add appropriate code  
- test with `npm test`

