export class Category {
    public _id: number;
    public name: string;

    constructor(id: number, name: string) {
        this._id = id;
        this.name = name;
    }
}
