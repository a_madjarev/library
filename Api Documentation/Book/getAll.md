# Get all books

Get the list of all books for authenticated users

**URL** : `/api/books/`

**Method** : `GET`

**Auth required** : YES

**Permissions required** : None

## Success Response

**Code** : `200 OK`

**Content examples**

```json
[
    {
        "_id": "5cfe61ba8c00c344a27019a8",
        "title": "Summer of Night",
        "author": "Dan Simmons",
        "publishDate": "1992-02-29T23:00:00.000Z",
        "isbn": "9785699236527",
        "category": {
            "_id": "5cfe5b0660325b32eefa57eb",
            "name": "Horror"
        },
        "date": "2019-06-10T13:57:14.330Z",
        "__v": 0
    },
    {
        "_id": "5cff53e9576bb32602b14cff",
        "title": "The Eye of the World",
        "author": "Robert Jordan",
        "publishDate": "1990-01-14T23:00:00.000Z",
        "isbn": "9783453180000",
        "category": {
            "_id": "5cff5326576bb32602b14cf9",
            "name": "Fantasy"
        },
        "date": "2019-06-11T07:10:33.450Z",
        "__v": 0
    },
    {
        "_id": "5cff53f3576bb32602b14d01",
        "title": "The Great Hunt",
        "author": "Robert Jordan",
        "publishDate": "1990-11-14T23:00:00.000Z",
        "isbn": "9780606343190",
        "category": {
            "_id": "5cff5326576bb32602b14cf9",
            "name": "Fantasy"
        },
        "date": "2019-06-11T07:10:43.235Z",
        "__v": 0
    },
    {
        "_id": "5cff54f9576bb32602b14d05",
        "title": "Caliban's War",
        "author": "James S. A. Corey",
        "publishDate": "2012-06-25T22:00:00.000Z",
        "isbn": "9788834729083",
        "category": {
            "_id": "5cff5455576bb32602b14d03",
            "name": "Sci-fi"
        },
        "date": "2019-06-11T07:15:05.861Z",
        "__v": 0
    },
    ...
]
```

## Error Responses

**Condition** : If there are no books in database

**Code** : `400 BAD REQUEST`

**Content** : 
```json
{
    "no_books": "There are no books"
}
```