const chai = require('chai');
const chaiHttp = require('chai-http');
const request = require('supertest');
const app = require('../../server');

const Category = require('../../models/Category');
const User = require('../../models/User');

let should = chai.should();
let token;

chai.use(chaiHttp);

describe('Create user', () => {
  it('should create new test user', (done) => {
    chai.request(app)
      .post('/api/users/register')
      .send({
        "name": 'new test user',
        "email": "newtestuser@newtestserver.com",
        "password": "password",
        "password2": "password"
      })
      .end((err, res) => {
        User
          .findOne({"email": "newtestuser@newtestserver.com"})
          .then(res => {
            res.role = 4;
            res.save()
          });
        done();
      });
  });
});

describe('Testing of the get all categories route GET /api/categories', () => {
  before('should login new test user', (done) => {
    chai.request(app)
      .post('/api/users/login')
      .send({
        "email": "newtestuser@newtestserver.com",
        "password": "password"
      })
      .end((err, res) => {
        token = res.body.token;
        done();
      });
  });

  it('should return status 200', (done) => {
    chai.request(app)
      .get('/api/categories')
      .set("Authorization", token)
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a('array');
        done();
      });
  });
});

describe('Testing of the creating new category route POST /api/categories', () => {
  before('should login new test user', (done) => {
    chai.request(app)
      .post('/api/users/login')
      .send({
        "email": "newtestuser@newtestserver.com",
        "password": "password"
      })
      .end((err, res) => {
        token = res.body.token;
        done();
      });
  });

  it('should create new category, and return status 200', (done) => {
    request(app)
      .post('/api/categories')
      .set("Authorization", token)
      .send({name: "new test category"})
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.have.property('name');
        res.body.should.have.property('name').eql('new test category');
        done();
      });
  });
});

describe('Remove testing categories', () => {
  it('should remove all categories with name new test category', (done) => {
    Category.deleteMany({"name": "new test category"}, function (err) {
    });
    done();
  })
});

describe('Remove testing user', () => {
  it('should remove all users with email newtestuser@newtestserver.com', (done) => {
    User.deleteMany({"email": "newtestuser@newtestserver.com"}, function (err) {
    });
    done();
  });
});