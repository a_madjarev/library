import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';

export interface User {
  id: string;
  email: string;
  name: string;
  role: number;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  API = 'http://localhost:5000/api/users';
  private authenticated = false;
  private headers: HttpHeaders;
  private user: User;

  constructor(private httpClient: HttpClient, private router: Router) { }

  register(name: string, email: string, password: string, password2: string) {
    return this.httpClient.post<any>(this.API + '/register', { name, email, password, password2 });
  }

  login(email: string, password: string) {
    return this.httpClient.post<any>(this.API + '/login', { email, password }).pipe(
      tap(resData => {
        localStorage.setItem('token', resData.token);
        this.setAuthHeaders(resData.token);
        this.getCurrentUser().subscribe(
          (user) => {
            this.user = user;
            this.authenticated = true;
            this.router.navigate(['/home']);
          }
        );
      })
    );
  }

  getCurrentUser() {
    return this.httpClient.get<User>(this.API + '/current', { headers: this.headers });
  }

  isAuthenticated() {
    return this.authenticated;
  }

  getAuthHeaders() {
    return this.headers;
  }

  setAuthHeaders(token: string) {
    this.headers = new HttpHeaders({
      Authorization: token
    });
  }

  getUsername() {
    if (this.user) {
      return this.user.name;
    }
  }

  hasRoleAdmin() {
    if (this.user) {
      return this.user.role === 4;
    }
  }

  logout() {
    this.authenticated = false;
    this.user = null;
    this.headers = null;
    this.router.navigate(['/login']);
    localStorage.removeItem('token');
  }

  autoLogin() {
    const token = localStorage.getItem('token');
    if (!token) {
      return;
    }

    this.setAuthHeaders(token);
    this.getCurrentUser().subscribe(
      (user) => {
        this.user = user;
        this.authenticated = true;
      }
    );
  }

}
