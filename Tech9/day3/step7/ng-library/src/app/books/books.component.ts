import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { Book } from './book.model';
import { BookService } from './book.service';

@Component({
  selector: 'library-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.scss']
})
export class BooksComponent implements OnInit {
  books$: Observable<Book[]>;

  constructor(private bookService: BookService) { }

  ngOnInit() {
    this.books$ = this.bookService.getBooks();
  }

}
