# Delete Category

Delete the category if exists. If this category is also part of any books, these books will be deleted.

**URL** : `/api/categories/:cat_id/`

**URL Parameters** : `cat_id` where `cat_id` is the ID of the Category in the database.

**Method** : `DELETE`

**Auth required** : YES

**Permissions required** : ADMIN

## Success Response

**Condition** : If the Category exists.

**Code** : `200 OK`

**Content examples**

```json
{
    "msg": "Category removed"
}
```

## Error Responses

**Condition** : If Category with requested ID doesn't exists.

**Code** : `400 BAD REQUEST`

**Content examples**

```json
{
    "msg": "Category does not exists"
}
```

### Or

**Condition** : Authorized User don have permissions.

**Code** : `403 FORBIDDEN`

`Unauthorized`
