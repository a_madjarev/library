const chai = require('chai');
const chaiHttp = require('chai-http');
const request = require('supertest');
const app = require('../../server');

const Book = require('../../models/Book');
const User = require('../../models/User');

let should = chai.should();
let token;

chai.use(chaiHttp);

describe('Create user', () => {
  it('should create new test user', (done) => {
    chai.request(app)
      .post('/api/users/register')
      .send({
        "name": 'new test user',
        "email": "newtestuser@newtestserver.com",
        "password": "password",
        "password2": "password"
      })
      .end((err, res) => {
        User
          .findOne({"email": "newtestuser@newtestserver.com"})
          .then(res => {
            res.role = 4;
            res.save()
          });
        done();
      });
  });
});

describe('Testing of the get all books route GET /api/books', () => {
  before('should login new test user', (done) => {
    chai.request(app)
      .post('/api/users/login')
      .send({
        "email": "newtestuser@newtestserver.com",
        "password": "password"
      })
      .end((err, res) => {
        token = res.body.token;
        done();
      });
  });

  it('should return status 200', (done) => {
    chai.request(app)
      .get('/api/books')
      .set("Authorization", token)
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a('array');
        done();
      });
  });
});

describe('Testing of the creating new book route POST /api/book', () => {
  before('should login new test user', (done) => {
    chai.request(app)
      .post('/api/users/login')
      .send({
        "email": "newtestuser@newtestserver.com",
        "password": "password"
      })
      .end((err, res) => {
        token = res.body.token;
        done();
      });
  });

  it('should create new book, and return status 200', (done) => {
    request(app)
      .post('/api/books')
      .set("Authorization", token)
      .send({
        "title": "new test book",
        "category": "5cfe3d17da3d9c7c84a2caae",
        "author": "test author",
        "isbn": "9780747409878",
        "publishDate": "Mon Jun 10 2019 13:46:13 GMT+0200"
      })
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.have.property('title');
        res.body.should.have.property('title').eql('new test book');
        res.body.should.have.property('author');
        res.body.should.have.property('author').eql('test author');
        res.body.should.have.property('category');
        res.body.should.have.property('isbn');
        res.body.should.have.property('publishDate');
        done();
      });
  });
});

describe('Remove testing books', () => {
  it('should remove all books with title new test category', (done) => {
    Book.deleteMany({"title": "new test book"}, function (err) {
    });
    done();
  });
});

describe('Remove testing user', () => {
  it('should remove all users with email newtestuser@newtestserver.com', (done) => {
    User.deleteMany({"email": "newtestuser@newtestserver.com"}, function (err) {
    });
    done();
  });
});