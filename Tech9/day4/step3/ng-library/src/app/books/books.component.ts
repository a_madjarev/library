import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { Book } from './book.model';
import { BookService } from './book.service';

@Component({
  selector: 'library-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.scss']
})
export class BooksComponent implements OnInit {
  books$: Observable<Book[]>;
  selectedBook: Book = new Book(null, null, null, null, null, null);

  constructor(private bookService: BookService) { }

  ngOnInit() {
    this.books$ = this.bookService.getBooks();
  }

  onBookDelete(book: Book) {
    this.selectedBook = Object.assign({}, book);
  }

  onBookDeleteSubmit() {
    this.bookService.deleteBook(this.selectedBook._id)
      .subscribe(
        () => {
          this.books$ = this.bookService.getBooks();
          this.selectedBook = new Book(null, null, null, null, null, null);
        },
        (error) => console.error(error)
      );
  }

}
