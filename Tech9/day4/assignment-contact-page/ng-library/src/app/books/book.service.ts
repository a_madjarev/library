import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Book } from './book.model';

@Injectable({
  providedIn: 'root'
})
export class BookService {
  API = 'http://localhost:5000/api/books';

  constructor(private httpClient: HttpClient) { }

  // Return Observable that wraps array of Books
  getBooks(): Observable<Book[]> {
    return this.httpClient.get<Book[]>(this.API);
  }

  // Update book if book already has an ID, save it otherwise and return Observable
  saveBook(book: Book): Observable<any> {
    return this.httpClient.post(this.API, book);
  }

  // Delete book by ID and return Observable
  deleteBook(bookId: number) {
    return this.httpClient.delete(this.API + '/' + bookId);
  }
}
