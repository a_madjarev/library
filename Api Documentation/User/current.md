# Show Current User

Get the details of the currently Authenticated User along with basic subscription information.

**URL** : `/api/users/current`

**Method** : `GET`

**Auth required** : YES

**Permissions required** : None

## Success Response

**Code** : `200 OK`

**Content examples**

For a User with ID 5cfe3cbcbda5637c00ba88f9 on the local database where that User has saved an
email address, name and role information.

```json
{
    "id": "5cfe3cbcbda5637c00ba88f9",
    "name": "Admin Admin",
    "email": "admin@test.com",
    "role": 4
}
```