import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { Category } from 'src/app/categories/category.model';
import { CategoryService } from 'src/app/categories/category.service';

@Component({
  selector: 'library-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.scss']
})
export class CategoryListComponent implements OnInit {
  categories$: Observable<Category[]>;

  constructor(private categoryService: CategoryService) { }

  ngOnInit() {
    this.categories$ = this.categoryService.getCategories();
  }

}
