import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';

import { Book } from 'src/app/books/book.model';
import { BookService } from 'src/app/books/book.service';
import { CategoryService } from './../../categories/category.service';

@Component({
  selector: 'library-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.scss']
})
export class BookListComponent implements OnInit, OnDestroy {
  books$: Observable<Book[]>;
  selectedCategoryId: number;
  categoryFilterSubscription: Subscription;

  constructor(private bookService: BookService, private categoryService: CategoryService) { }

  ngOnInit() {
    this.books$ = this.bookService.getBooks();
    this.categoryFilterSubscription = this.categoryService.categoryFilterChanged.subscribe(
      (selectedCategoryId) => {
        this.selectedCategoryId = selectedCategoryId;
      }
    );
  }

  ngOnDestroy() {
    this.categoryFilterSubscription.unsubscribe();
  }

}
