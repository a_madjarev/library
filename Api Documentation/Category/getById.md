# Get category by id

Get the one category by it's ID

**URL** : `/api/categories/:cat_id`

**Method** : `GET`

**Auth required** : YES

**Permissions required** : None

## Success Response

**Code** : `200 OK`

**Content examples**

```json
{
    "_id": "5cfe5b0660325b32eefa57eb",
    "name": "Horror",
    "date": "2019-06-10T13:28:38.853Z",
    "__v": 0
}
```

## Error Responses

**Condition** : If there is no category with that ID in database

**Code** : `400 BAD REQUEST`

**Content** : 
```json
{
    "no_category": "Requested category does not exists"
}
```