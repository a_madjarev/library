import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';

import { Book } from 'src/app/books/book.model';
import { BookService } from 'src/app/books/book.service';

@Component({
  selector: 'library-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.scss']
})
export class BookListComponent implements OnInit {
  books$: Observable<Book[]>;
  @Input() selectedCategoryId: number;

  constructor(private bookService: BookService) { }

  ngOnInit() {
    this.books$ = this.bookService.getBooks();
  }

}
