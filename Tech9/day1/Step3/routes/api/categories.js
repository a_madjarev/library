const express = require("express");
const router = express.Router();

// @route   GET api/categories/test
// @desc    Tests post route
// @access  Public
router.get("/test", (req, res) => {
  res.json({ msg: "Categories works" });
});

module.exports = router;
