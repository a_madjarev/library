import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  API = 'http://localhost:5000/api/users';
  private authenticated = false;

  constructor(private httpClient: HttpClient) { }

  register(name: string, email: string, password: string, password2: string) {
    return this.httpClient.post<any>(this.API + '/register', { name, email, password, password2 });
  }

  isAuthenticated() {
    return this.authenticated;
  }
}
