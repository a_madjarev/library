Day 1: Introduction, environment setup, startup application using node.js with express framework 
==================================================================================

## Setup Environment
* [Download and install Node.js LTS](https://nodejs.org/en/download/)
* [Download and install VSCode](https://code.visualstudio.com/download)
* [Download and install Postman](https://www.getpostman.com/downloads/)
* [Download and install Git](https://git-scm.com/downloads)
## VSCode setup
Install plugins

* Bracket Pair Colorizer  
* Prettier - Code formatter  
* Live Server  
* Node.js Modules Intellisense
* TSLint  

In settings.json  
`{`  
`"editor.tabSize": 2,`  
`"editor.wordWrap": "on",`  
`"editor.formatOnSave": true,`  
`}`  
For Windows also add  
`"terminal.integrated.shell.windows": "C:\Program Files\Git\bin\bash.exe"`
## MongoDB
For database we are going to use NoSQL database called MongoDB. We can download and run it locally, but we can also use MongoDB Atlas, a cloud version of database.
Go to [MongoDB Download Center](https://www.mongodb.com/download-center), register and confirm your account. After that, login to [MongoDB Cloud](https://cloud.mongodb.com), and now we are going to create new cluster.  
  
* Click build a cluster  
* Choose one of free tier for AWS. In free version we can have one free cluster for use.  
* Cluster will be created. It'll take 7-10 minutes to gain access.  
* After creation our cluster will be accessible.  
* Create two users, one user with admin privileges, and one user that can read and write to any database.  
* We also need to whitelist our IP for database access. Click IP Whitelist, Add IP Address, select ALLOW ACCESS FROM ANYWHERE, and Confirm  
* If we want tighter security, this can be modified of course.  

## Using Git, creating repository, configuration and connecting to project
For our Git repository we are going to use Bitbucket.  
  
* Create and confirm your Bitbucket account  
* Create repository called MyLibrary on Bitbucket for our application  
* Create .gitignore file and put ignore rules. Gitignore rules can be created on http://gitignore.io, enter following in text box:   
	* VisualStudioCode  
	* Windows  
	* Linux  
	* Node  
	* Angular  
* Paste result in text box on Bitbucket and commit with any message  
* In terminal navigate to folder where you want to put project and clone the project with following command  
	* `git clone https://{username}@bitbucket.org/{username}/{repo-name}.git`  
	* Enter your Bitbucket password if you're created private repository  
	* Correct path we can see if we click on clone button on our Bitbucket repository  
* Setup your git name and email with following command  
	* `git config --global user.name "YOUR NAME"`  
	* `git config --global user.email "YOUR EMAIL"`  
* Now we have our version controlled directory where we are going to build our application  

## More about Git
We don’t want to put all our code to master branch, so we’re going to introduce a branching model for further development. On our master branch we should only have code that’s fully written, tested and ready to release.
For development, we are going to use new branch called “develop”. On this branch we will merge all our features.
When we are ready for release we can later merge this branch to our master branch.  
  
* In root of our project, create new branch with following command  
	* `git checkout -b "develop"`  
* If we enter `git status`, we will see that we are on new branch called "develop"  

### Initialization of project
Now we are going to create new node.js project and install all necessary dependencies  
  
* Open the root of our project with VSCode. We should have one file called .gitignore, and maybe one more called README.MD if we checked that option during repository creation    
* Open terminal in VSCode, or standard linux terminal in the root of project  
* Initialize new project with `npm init`. Follow wizard and enter required information's. No need to change anything during this process, except entry point where we are going to user `server.js` instead of `index.js`    
* After completion, a `package.json` file will be created in our root directory  
* Install required dependencies with `npm install dependency-name`. We can also chain our dependencies with white space `npm install dep1 dep2 dep3...`    
* List of required dependencies  
	* express  
	* mongoose  
	* passport  
	* passport-jwt  
	* jsonwebtoken  
	* body-parser  
	* bcryptjs  
	* validator  
	* is-isbn  
* Install dev dependencies with `npm install -D dependency-name`    
* List of dev dependencies  
	* nodemon  
* Now create `server.js` file in our project root and fill with following code:  

`const express = require('express');`  
`const app = express();`  
`app.get('/', (req, res) => res.send('Hello'));`  
`const port = process.env.PORT || 5000;`  
``app.listen(port, () => console.log(`Server running on port ${port}`));``  

* Start app with `node server` in our project root and open http://localhost:5000 in browser  
  
### First commit
Now we have new untracked files in our project. Before commiting this changes to our git repository, first we need to track this files.  

* Enter `git status` to see list of untracked changes  
* Enter `git add .` to track all untracked files in our project  
* Enter `git status` again to see difference  
* Enter `git commit -m "Project init"` to commit this changes  
* Enter `git push origin develop` to push our code on Bitbucket repository  
* Check develop branch on Bitbucket to see changes  

### Git branching
From here we can create further branches and when we finish coding and testing, we will merge them to development branch.  

New branch is created with following command `git checkout -b "name-of-branch"`  

Switching to existing branch is done without `-b` flag: `git checkout name-of-branch`  

For new feature, we can use `feature/name-of-feature`    
For bugfix, we can use `bugfix/name-of-bugfix`  

For easier maintenance, on feature branch, we will only enter code that is related to that feature. If we already pushed our code on remote repository, and a bug is discovered after, we should checkout on that feature branch, create new bugfix branch, fix the bug, merge this bugfix branch back to the feature branch, and then push our feature branch again to remote repository with following command `git push origin feature/name-of-feature`  

Merging of branches is done with git merge command, for example if we want to merge bugfix branch to our feature branch, we should checkout to our feature branch with following command `git checkout feature/name-of-feature`, then merge our bugfix branch into feature branch with `git merge bugfix/name-of-bugfix`  
If we receive conflicts, we should fix them and make new commit called "fixed conflicts". If there are no conflicts, branches will be auto merged.  
After that we will once again push our feature branch to remote repository with following command `git push origin feature/name-of-feature`  

Before any new branch introduction, the best practice is to first pull all latest code from remote repository regarding that branch if that branch already exist on remote repository. For example `git pull origin develop`. This will download all latest changes from develop branch. In group projects this is important, because it'll reduce significantly possible conflicts.  

About merging our features to develop branch, we will talk a little later when we introduce code reviewing and merge requests.  

### Connecting our application with MongoDB
If you are not already on develop branch, checkout to develop branch, and pull latest changes if they exists  

* `git checkout develop`  
* `git pull origin develop`    

Create new feature branch with following command    

* `git checkout -b "feature/as-a-developer-i-want-to-connect-my-application-with-database"`    

Now we can continue our work on this branch.  

* In project root create new folder named config, and there create keys.js file where  we will store our credentials, configurations, and paths we need in our application  
* In server.js add mongoose dependency  
* Import DB Info from keys.js file  
* Connect to MongoDB  
* If everything works, commit and push feature branch on remote repository  

### About code review and pull requests

- Open repository on Bitbucket and enter settings  
- Under branching model, for development branch choose “Use specific branch” and then choose develop branch. Save settings  
- Click on pull requests, then “Create pull request”  
- Choose feature branch you want to merge with develop branch  
- Enter title and description to help reviewer with the changes in the code.  
- We can also choose a specific user to review our code, and after that click “Create pull request”. Do not close the branch, because if some bug is introduced later with this pull request, it’s a good option to fix it on this branch.  
- After code is reviewed pull request will be merged or declined  
- When merge is done, checkout to develop branch through terminal, and pull the code with command: git pull origin develop  
- Now we have latest changes on develop branch, and we can start working on new feature from this point creating new feature branch.  

This is a basic git flow used on most projects, so feel free to continue using it, but for time limitation, current development of library app will ignore it.  
It’s strongly recommended to use this system in group project from next week, because this way you’ll have less conflicts that needs to be fixed.  

## Creating test routes

- Create routes folder in root of project, and inside it, create another folder called api.  
- Inside api folder create three js files: users.js, books.js and categories.js  
- Import those files in server.js file and add them to the app instance  
- Test with postman  