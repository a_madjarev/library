# Delete Book

Delete the book if exists.

**URL** : `/api/books/:book_id/`

**URL Parameters** : `book_id` where `book_id` is the ID of the Book the database.

**Method** : `DELETE`

**Auth required** : YES

**Permissions required** : ADMIN

## Success Response

**Condition** : If the Book exists.

**Code** : `200 OK`

**Content examples**

```json
{
    "msg": "Book removed"
}
```

## Error Responses

**Condition** : If Book with requested ID doesn't exists.

**Code** : `400 BAD REQUEST`

**Content examples**

```json
{
    "msg": "Book does not exists"
}
```

### Or

**Condition** : Authorized User don have permissions.

**Code** : `403 FORBIDDEN`

`Unauthorized`