import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';

import { Category } from 'src/app/categories/category.model';
import { CategoryService } from 'src/app/categories/category.service';

@Component({
  selector: 'library-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.scss']
})
export class CategoryListComponent implements OnInit {
  categories$: Observable<Category[]>;
  @Output() categorySelected = new EventEmitter<number>();
  selectedCategoryId: number = null;

  constructor(private categoryService: CategoryService) { }

  ngOnInit() {
    this.categories$ = this.categoryService.getCategories();  
  }

  onCategorySelect(categoryId: number) {
    this.selectedCategoryId = categoryId;
    this.categorySelected.emit(categoryId);
  }

}
