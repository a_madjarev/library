const mongoose = require("mongoose");
const userRoles = require("../config/roles");
const Schema = mongoose.Schema;

// Create Schema
const UserSchema = new Schema({
  name: {
    type: String,
    required: true,
    allowNull: false
  },
  email: {
    type: String,
    unique: true,
    required: true,
    allowNull: false
  },
  password: {
    type: String,
    required: true,
    allowNull: false
  },
  role: {
    type: Number,
    default: userRoles.user,
    required: true,
    allowNull: false
  },
  date: {
    type: Date,
    default: () => {
      return new Date();
    }
  }
});

module.exports = User = mongoose.model("users", UserSchema);
