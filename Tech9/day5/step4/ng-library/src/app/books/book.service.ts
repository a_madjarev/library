import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Book } from './book.model';
import { AuthService } from '../auth.service';

@Injectable({
  providedIn: 'root'
})
export class BookService {
  API = 'http://localhost:5000/api/books';

  constructor(private httpClient: HttpClient,  private authService: AuthService) { }

  // Return Observable that wraps array of Books
  getBooks(): Observable<Book[]> {
    return this.httpClient.get<Book[]>(this.API, { headers: this.authService.getAuthHeaders() });
  }

  // Update book if book already has an ID, save it otherwise and return Observable
  saveBook(book: Book): Observable<any> {
    return this.httpClient.post(this.API, book, { headers: this.authService.getAuthHeaders() });
  }

  // Delete book by ID and return Observable
  deleteBook(bookId: number) {
    return this.httpClient.delete(this.API + '/' + bookId, { headers: this.authService.getAuthHeaders() });
  }
}
